<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::factory()->count(20)->make();

        $products = [];
        $num = 0;
        $sub_products = [];
        $products = Product::pluck('id')->toArray();
        foreach ($orders as $key => $order) {
            $order = Order::create($order->toArray());
            $sub_products = array_rand($products, 7);
            for ($i = 0; $i < rand(1, 6); $i++) {
                $num = $sub_products[array_rand($sub_products)];
                foreach (array_keys($sub_products, $num) as $key) {
                    unset($sub_products[$key]);
                }
                if ($num != 0) {
                    OrderProduct::create(['order_id' => $order->id, 'product_id' => $num]);
                }
            }
        }
    }
}
