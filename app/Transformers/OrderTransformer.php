<?php
namespace App\Transformers;

use App\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    public function transform(Order $order)
    {
        $products = $order->products;

        foreach ($products as $key => $product) {
            $products[$key] = (new ProductTransformer)->transform($product);
        }

        return [
            'id' => $order->id,
			'payment_type' => $order->payment_type,
			'price' => $order->price,
			'user' => (new UserTransformer)->transform($order->user),
            'products' => $products,
			'create_date' => $order->created_at,
			'update_date' => $order->updated_at
        ];
    }
}
