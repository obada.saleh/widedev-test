<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\User;
use Illuminate\Http\Request;
use App\Transformers\OrderTransformer;
use Tymon\JWTAuth\Facades\JWTAuth;

class OrderController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $user = User::find($request->query('user_id'));
        $orders = [];
        if ($user) {
            $orders = $user->orders;
            foreach ($orders as $key => $order) {
                $orders[$key] = (new OrderTransformer)->transform($order);
            }
            return $this->sendResponse($orders, 'User Orders retrieved successfully', 200);
        } else {
            return $this->sendError('Please Send User ID', '', 400);
        }
    }

    public function store(OrderRequest $request)
    {

        $order = Order::create(array_merge(
            $request->validated(),
            ['user_id' => JWTAuth::parseToken()->authenticate()->id]
        ));

        foreach ($request->products as $key => $product) {
            OrderProduct::create(['order_id' => $order->id , 'product_id' => $product]);
        }

        $order = (new OrderTransformer())->transform($order);
        return $this->sendResponse($order, 'Order created successfully.', 201);
    }

    public function show(Order $order)
    {
        $order = (new OrderTransformer)->transform($order);
        return $this->sendResponse($order, 'Order retrieved successfully', 200);
    }

    public function update(Request $request, Order $order)
    {
        if($product_id = $request->product_id) {
            $product_id = $request->product_id;
            $products = $order->products;
            foreach ($products as $key => $product) {
                if($product->id == $product_id) {
                    return $this->sendError('Product already exist on this order', '', 400);
                }
            }
            OrderProduct::create(['order_id' => $order->id , 'product_id' => $product_id]);
            $order = Order::find($order->id);
            $order = (new OrderTransformer)->transform($order);
            return $this->sendResponse($order, 'Order retrieved successfully', 200);
        }
    }
}
